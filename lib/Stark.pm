package Stark;

use strict;
use warnings;
use Moo;
use Sub::Util 'set_subname';
use Exporter;
use JSON;
use Data::Dumper;
use Plack::Request;
use Carp;

sub import
{
	my ( $class, $caller ) = ( shift, caller );

	my $self = $class->new;
	monkey_patch( $caller, 'get', sub { $self->get(@_) } );
	monkey_patch( $caller, 'engage', sub { $self->engage(@_) } );
}

sub BUILD
{
	my ( $self, $args )  = @_;

}

sub monkey_patch
{
  my ($class, %patch) = @_;
  no strict 'refs';
  no warnings 'redefine';
  *{"${class}::$_"} = set_subname("${class}::$_", $patch{$_}) for keys %patch;
}

=head example of routes internal structure
$self->{routes}->{chunks} = {
	/ => {
		sub => ref1, # /
		chunks => {
			foo => {
				sub => ref2 # /foo
				chunks => {
					* => {
						sub => ref3, # /foo/*
						chunks => {
							bar => {
								sub => ref5, # /foo/*/bar
								chunks => {},
							}
						}
					},
					bar => {
						sub => ref4,  # /foo/bar
						chunks => {},
					}
				}
			}
		}
	}
}
=cut

sub route_add
{
	my ( $self, $path, $sub ) = @_;
	my @chunks;
	my $full_path = $path;
	my $regex_url;
	warn "ADD ROUTE FOR $path";
	while ( $path =~ s/^(.*)\/(.*?)$/$1/ )
	{
		push @chunks, $2;
	}
	push @chunks, '/'; #Always include default route
	@chunks = reverse @chunks;
	$self->{routes} //= {};
	my $prev = \$self->{routes};
	for ( @chunks )
	{
		next unless $_;
		$$prev->{chunks}->{$_} //= {};
		$prev = \$$prev->{chunks}->{$_};
	}
	$$prev->{sub} = $sub;
}

sub get
{
	my ( $self, $path, $sub ) = @_;
	$self->route_add( $path, $sub );
}

sub router
{
	my ( $self, $request_uri ) = @_;
	warn $request_uri;
	my $path = $request_uri;
	my @request_chunks;
	while ( $path =~ s/^(.*)\/(.*?)$/$1/ )
	{
		push @request_chunks, $2;
	}
	push @request_chunks, '/';
	@request_chunks = reverse @request_chunks;
	my @args;
	my $sub;
	my $prev = \$self->{routes};
	for ( @request_chunks )
	{
		warn "CHECKING CHUNK $_";
		if ( $$prev->{chunks}->{$_} )
		{
			warn "Found a matching chunk";
			if ( $$prev->{chunks}->{$_}->{sub} )
			{
				warn "chunk has sub";
				if ( $sub )
				{
					warn "matching chunk has a sub need to execute previous sub";
					$$sub->($self, @args);
				}
				$sub = \$$prev->{chunks}->{$_}->{sub};
			}
			$prev = \$$prev->{chunks}->{$_};
		}
		elsif ( $$prev->{chunks}->{'*'} )
		{
			warn "No matching chunk but did hit a wildcard push to \@args";
			push @args, $_;
			if ( $$prev->{chunks}->{'*'}->{sub} )
			{
				if ( $sub )
				{
					warn "matching chunk has a sub need to execute previous sub";
					$$sub->($self, @args);
				}
				$sub = \$$prev->{chunks}->{'*'}->{sub};
			}
			$prev = \$$prev->{chunks}->{'*'};
		}
		else
		{
			warn "no match found";
			return;
		}
	}
	if ( $sub )
	{
		$$sub->($self, @args);
	}
	else
	{
		warn "NO ROUTE MATCHED";
	}
}

sub output
{
	my ( $self, $args ) = @_;
	for ( keys %$args )
	{
		$self->{output}->{$_} = $args->{$_};
	}
	return $self->{output} // {};
}

sub engage
{
	my $self = shift;
	return sub {
		my $env = shift;
		my $req = Plack::Request->new($env);
		$self->{output} = undef;
		$self->router( $env->{REQUEST_URI} );
		my $code = '200';
		my $res = $req->new_response( $code );
		$res->content_type('text/json');
		$res->body( encode_json( $self->output ) );
		$res->finalize;
	};
}

1;
