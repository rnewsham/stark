#!/usr/bin/env perl

use strict;
use warnings;
use lib 'lib';

use Stark;

get '/' => sub {
	warn '/ method';
};

get '/foo' => sub {
	warn '/foo method';
	my $c = shift;
	$c->output( { foo => 'bar' } );
};

get '/foo/*' => sub {
	my ( $c, $id ) = @_;
	warn '/foo/* method $id=' . $id;
	$c->output( { foo_root_called => $id } );
};

get '/foo/bar' => sub {
	my ( $c ) = @_;
	warn '/foo/bar method';
	$c->output( { foo_bar_called => 'FooBar' } );
};

get '/foo/*/bar' => sub {
	my ( $c, $foo_id )  = @_;
	warn '/foo/*/bar method foo_id=' . $foo_id;
	$c->output( { bar => $foo_id } );
};

$ENV{'REQUEST'} = shift;
engage;

