# Stark

## Description

Stark is a JSON API framework based on Mojolicious::Lite, Catalyst and Dancer.

This is intended to offer as minimal a framework as possible. It is not a full web framework like it's inspirations

## Example

### Stark script

```perl

#!/usr/bin/env perl

use strict;
use warnings;
use lib 'lib';

use Stark;

get '/' => sub {
};

get '/foo' => sub {
	my $c = shift;
	$c->output( { foo => 'bar' } );
};

get '/foo/*' => sub {
	my ( $c, $id ) = @_;
	$c->output( { foo_root => $id } );
};

get '/foo/bar' => sub {
	my ( $c ) = @_;
	$c->output( { foo_bar => 'FooBar' } );
};

get '/foo/*/bar' => sub {
	my ( $c, $foo_id )  = @_;
	$c->output( { bar => $foo_id } );
};

$ENV{'REQUEST'} = shift;
engage;
	
```

### Lauch server
```bash
$ plackup test.pl -p 5000 -r

```

### Methods exposed and output produced

#### http://localhost:5000/
	{}
	
#### http://localhost:5000/foo
	{"foo":"bar"}
	
#### http://localhost:5000/foo/bar
	{"foo":"bar","foo_bar":"FooBar"}

#### http://localhost:5000/foo/123
	{"foo_root":"123","foo":"bar"}

#### http://localhost:5000/foo/123/bar
	{"bar":"1","foo_root":"123","foo":"bar"}
