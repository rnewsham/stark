# TODO

## Method handling
		
Need to support 

- POST
- PUT
- DELETE
- JSON in request body

## Header handling

Need to handle request and response header values 

## Autoload sub controllers

Allow splitting methods across multiple files for ease of organising code structure.

## Define standard directory structure

Set out structure for plugins, controllers and any other components needed for ease of updating

## Application creation helpers

Helper scripts for populating basic components with basic scaffolding for ease of getting started

## Plugin support

- auth ( user/pass | preshared key | oauth )
- cache
- jwt

## Debugging/logging handler

Log handler with configurable log levels 

## Performance monitoring ( req per seconds calc )

Track and report time of each request processed